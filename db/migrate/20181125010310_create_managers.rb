class CreateManagers < ActiveRecord::Migration[5.2]
  def change
    create_table :managers do |t|
      t.string :first_Name
      t.string :last_Name
      t.string :email
      t.string :username
      t.string :password
      t.string :job_Title
      t.string :executive_Id
      t.string :underlying_Id

      t.timestamps
    end
  end
end
