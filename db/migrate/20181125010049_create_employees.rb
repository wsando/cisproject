class CreateEmployees < ActiveRecord::Migration[5.2]
  def change
    create_table :employees do |t|
      t.string :first_Name
      t.string :last_Name
      t.string :email
      t.string :username
      t.string :password
      t.string :job_Title
      t.string :executive_Privilege
      t.string :underlying_ID

      t.timestamps
    end
  end
end
