class CreateCustomers < ActiveRecord::Migration[5.2]
  def change
    create_table :customers do |t|
      t.string :first_Name
      t.string :middle_Name
      t.string :last_Name
      t.string :email
      t.string :address
      t.string :zip_Code
      t.string :city
      t.string :country

      t.timestamps
    end
  end
end
