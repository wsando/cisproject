class Employee < ApplicationRecord
 has_many :quotes
  has_many :customers
  belongs_to :managers
end
