json.extract! manager, :id, :first_Name, :last_Name, :email, :username, :password, :job_Title, :executive_Id, :underlying_Id, :created_at, :updated_at
json.url manager_url(manager, format: :json)
