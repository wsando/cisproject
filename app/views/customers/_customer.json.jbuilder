json.extract! customer, :id, :first_Name, :middle_Name, :last_Name, :email, :address, :zip_Code, :city, :country, :created_at, :updated_at
json.url customer_url(customer, format: :json)
