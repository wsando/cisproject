json.extract! employee, :id, :first_Name, :last_Name, :email, :username, :password, :job_Title, :executive_Privilege, :underlying_ID, :created_at, :updated_at
json.url employee_url(employee, format: :json)
