##forcing the user to redirect to the login page if the user was not logged in https://guides.railsgirls.com/devise

class ApplicationController < ActionController::Base
before_action :authenticate_user!
end
