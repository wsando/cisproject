Rails.application.routes.draw do
  devise_for :users
  resources :quotes
  resources :managers
  resources :employees
  resources :customers
  resources :cars
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
