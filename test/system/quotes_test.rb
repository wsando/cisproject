require "application_system_test_case"

class QuotesTest < ApplicationSystemTestCase
  setup do
    @quote = quotes(:one)
  end

  test "visiting the index" do
    visit quotes_url
    assert_selector "h1", text: "Quotes"
  end

  test "creating a Quote" do
    visit quotes_url
    click_on "New Quote"

    fill_in "Customer First Name", with: @quote.customer_First_Name
    fill_in "Customer Last Name", with: @quote.customer_Last_Name
    fill_in "Quote Number", with: @quote.quote_Number
    fill_in "Sales Person Id", with: @quote.sales_Person_Id
    fill_in "Sales Person Name", with: @quote.sales_Person_Name
    click_on "Create Quote"

    assert_text "Quote was successfully created"
    click_on "Back"
  end

  test "updating a Quote" do
    visit quotes_url
    click_on "Edit", match: :first

    fill_in "Customer First Name", with: @quote.customer_First_Name
    fill_in "Customer Last Name", with: @quote.customer_Last_Name
    fill_in "Quote Number", with: @quote.quote_Number
    fill_in "Sales Person Id", with: @quote.sales_Person_Id
    fill_in "Sales Person Name", with: @quote.sales_Person_Name
    click_on "Update Quote"

    assert_text "Quote was successfully updated"
    click_on "Back"
  end

  test "destroying a Quote" do
    visit quotes_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Quote was successfully destroyed"
  end
end
